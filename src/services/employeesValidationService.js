export default {

    validate(data) {

        var errors = []
        var isValid = true


        if (data.firstLastName == "") {
            isValid = false
            errors.push({
                text: 'El campo primer apellido es requerido.'
            })
        }

        if (!data.firstLastName.match(/^[A-Z]*$/)) {
            isValid = false
            errors.push({
                text: 'En el campo primer apellido solo se permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ.'
            })
        }

        if (data.firstLastName.length >= 20) {
            isValid = false
            errors.push({
                text: 'En el campo primer apellido la longitud máxima es de 20 letras.'
            })
        }

        if (data.firstLastName == "") {
            isValid = false
            errors.push({
                text: 'El campo segundo apellido es requerido.'
            })
        }

        if (!data.secondLastName.match(/^[A-Z]*$/)) {
            isValid = false
            errors.push({
                text: 'En el campo segundo apellido solo se permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ.'
            })
        }

        if (data.secondLastName.length >= 20) {
            isValid = false
            errors.push({
                text: 'En el campo segundo apellido la longitud máxima es de 20 letras.'
            })
        }

        if (data.firstLastName == "") {
            isValid = false
            errors.push({
                text: 'El campo primer nombre es requerido.'
            })
        }

        if (!data.firstName.match(/^[A-Z]*$/)) {
            isValid = false
            errors.push({
                text: 'En el campo primer nombre solo se permiten caracteres de la A a la Z, mayúscula, sin acentos ni Ñ.'
            })
        }

        if (data.firstName.length >= 20) {
            isValid = false
            errors.push({
                text: 'El campo primer nombre debe tener una longitud máxima es de 20 letras.'
            })
        }

        if (!data.otherNames.match(/^[A-Z]*$/)) {
            isValid = false
            errors.push({
                text: 'El campo otros nombres solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ.'
            })
        }

        if (data.otherNames.length >= 50) {
            isValid = false
            errors.push({
                text: 'El campo otros nombres debe de tener una longitud máxima de 50 letras.'
            })
        }

        if (data.country == "") {
            isValid = false
            errors.push({
                text: 'El campo país es requerido.'
            })
        }

        if (data.documentType == "") {
            isValid = false
            errors.push({
                text: 'El campo típo de identificación es requerido.'
            })
        }

        if (data.documentId == "") {
            isValid = false
            errors.push({
                text: 'El campo documento de identificación es requerido.'
            })
        }

        if (data.documentId.length >= 20) {
            isValid = false
            errors.push({
                text: 'El campo documentId debe tener una longitud máxima de 20 letras.'
            })
        }

        if (data.email == "") {
            isValid = false
            errors.push({
                text: 'El campo email es requerido.'
            })
        }

        if (data.email.length >= 300) {
            isValid = false
            errors.push({
                text: 'El email debe de tener una longitud máxima de 300 caracteres'
            })
        }

        // if (new Date(data.admissionDate).getTime() > new Date().getTime()) {
        //     isValid = false
        //     errors.push({
        //         text: 'La fecha de ingreso no puede ser superior a la fecha actual.'
        //     })
        // }

        var oneMonthOld = new Date();
        oneMonthOld.setDate(oneMonthOld.getDate() - 31);

        if (new Date(data.admissionDate).getTime() < oneMonthOld.getTime()) {
            isValid = false
            errors.push({
                text: 'La fecha de ingreso no puede ser inferior a 30 días.'
            })
        }

        return {
            errors,
            isValid: isValid
        }

    }

}