import Vue from 'vue'
import Router from 'vue-router'
import Employees from '@/components/Employees'
import EmployeesAdd from '@/components/EmployeesAdd'
import EmployeesView from '@/components/EmployeesView'

Vue.use(Router)

export default new Router({
    routes: [{
        path: '/',
        name: 'Employees',
        component: Employees
    }, {
        path: '/add',
        name: 'add',
        component: EmployeesAdd
    }, {
        path: '/view/:id',
        name: 'view',
        component: EmployeesView
    }]
})